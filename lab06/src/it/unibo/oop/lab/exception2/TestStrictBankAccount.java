package it.unibo.oop.lab.exception2;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * JUnit test to test
 * {@link StrictBankAccount}.
 * 
 */
public class TestStrictBankAccount {

	/*
	 * 1) Creare due StrictBankAccountImpl assegnati a due AccountHolder a
	 * scelta, con ammontare iniziale pari a 10000 e nMaxATMTransactions=10
	 * 
	 * 2) Effetture un numero di operazioni a piacere per verificare che le
	 * eccezioni create vengano lanciate soltanto quando opportuno, cioè in
	 * presenza di un id utente errato, oppure al superamento del numero di
	 * operazioni ATM gratuite.
	 */
    
    private StrictBankAccount acc1;
    private StrictBankAccount acc2;
    private AccountHolder hol1;
    private AccountHolder hol2;
    
    @Before
    public void before() {
    	hol1 = new AccountHolder("Mario", "Rossi", 1);
    	acc1 = new StrictBankAccount(hol1.getUserID(), 10000, 10);
    	
    	hol2 = new AccountHolder("Luigi", "Verdi", 2);
    	acc2 = new StrictBankAccount(hol2.getUserID(), 10000, 10);
    }
    
    @Test
    public void testCanDeposit() {
    	acc1.deposit(hol1.getUserID(), 100);
    	assertEquals(10100, acc1.getBalance(), 0);
    	
    	acc1.deposit(hol1.getUserID(), 300);
    	assertEquals(10400, acc1.getBalance(), 0);
    }
    
	@Test
    public void testCanWithdraw() {
    	acc1.withdraw(hol1.getUserID(), 100);
    	assertEquals(9900, acc1.getBalance(), 0);
    }
    
    @Test(expected = WrongAccountHolderException.class)
    public void testThrowsExceptionOnWithdrawUsingWrongAccount() {
    	acc1.withdraw(hol2.getUserID(), 100);
    }
    
    @Test(expected = WrongAccountHolderException.class)
    public void testThrowsExceptionOnDepositUsingWrongAccount() {
    	acc1.deposit(hol2.getUserID(), 100);
    }
    
    @Test(expected = NotEnoughFundsException.class)
    public void testThrowsExceptionOnNotEnoughFunds() {
    	acc1.withdraw(hol1.getUserID(), 20000);
    }
    
    @Test(expected = TransactionsOverQuotaException.class)
    public void testThrowsExceptionOnTransactionsOverQuota() {
    	// limit is 10
    	for (int i = 0; i <= 11; i++) {
    		acc1.withdrawFromATM(hol1.getUserID(), 10);
    	}
    }
}
