package it.unibo.oop.lab.exception2;

public class TransactionsOverQuotaException extends RuntimeException {
	
	private double amount;
	private double limit;
	
	public TransactionsOverQuotaException(double amount, double limit) {
		this.amount = amount;
		this.limit = limit;
	}

	public String getMessage() {
		return "Transaction count exceeds quota. "
				+ this.amount + " exceeds the "
				+ this.limit + " limit.";
	}

}
