package it.unibo.oop.lab.exception2;

public class NotEnoughFundsException extends RuntimeException {
	
	private double required;
	private double available;
	
	public NotEnoughFundsException(double required, double available) {
		this.required = required;
		this.available = available;
	}
	
	public String getMessage() {
		return "Not enough funds. "
				+ this.required + " is required but only "
				+ this.available + " is available.";
	}

}
