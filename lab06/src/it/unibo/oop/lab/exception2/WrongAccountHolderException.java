package it.unibo.oop.lab.exception2;

public class WrongAccountHolderException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public WrongAccountHolderException() {
		super();
	}
	
	public String getMessage() {
		return "Unauthorized account holder.";
	}
}
