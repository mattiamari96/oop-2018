package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Example class using {@link List} and {@link Map}.
 * 
 */
public final class UseCollection {

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	ArrayList<Integer> arr = new ArrayList<>();
    	
    	for (int i = 1000; i < 2000; i++) {
    		arr.add(i);
    	}
    	
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	LinkedList<Integer> llist = new LinkedList<>(arr);
    	
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	int tmp = arr.get(0);
    	arr.set(0, arr.get(arr.size() - 1));
    	arr.set(arr.size() - 1, tmp);
    	
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    	for (int e : arr) {
    		System.out.print(e + ", ");
    	}
    	System.out.println("");
    	
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
    	long time = System.nanoTime();
    	
    	for (int i = 2001; i <= 102000; i++) {
    		arr.add(0, i);
    	}
    	
    	time = System.nanoTime() - time;
    	System.out.println("Add to arraylist: " + time / 1_000_000 + "ms");
    	
    	
    	time = System.nanoTime();
    	
    	for (int i = 2001; i <= 102000; i++) {
    		llist.addFirst(i);
    	}
    	
    	time = System.nanoTime() - time;
    	System.out.println("Add to linkedlist: " + time / 1_000_000 + "ms");
    	
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
    	time = System.nanoTime();
    	int idx = arr.size() / 2;
    	
    	for (int i = 0; i < 1000; i++) {
    		arr.get(idx);
    	}
    	
    	time = System.nanoTime() - time;
    	System.out.println("Read from arraylist: " + time / 1_000_000 + "ms");
    	
    	
    	time = System.nanoTime();
    	idx = llist.size() / 2;
    	
    	for (int i = 0; i < 1000; i++) {
    		llist.get(idx);
    	}
    	
    	time = System.nanoTime() - time;
    	System.out.println("Read from linkedlist: " + time / 1_000_000 + "ms");
    	
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
    	Map<String, Long> popu = new HashMap<>();
    	popu.put("Africa", 1_110_635_000L);
    	popu.put("Americas", 972_005_000L);
    	popu.put("Antartica", 0L);
    	popu.put("Asia", 4_298_723_000L);
    	popu.put("Europe", 742_452_000L);
    	popu.put("Oceania", 38_304_000L);
    	
        /*
         * 8) Compute the population of the world
         */
    	System.out.println("Total population: " + popu.values().stream().reduce(0L, (a, b) -> a+b));
    }
}
