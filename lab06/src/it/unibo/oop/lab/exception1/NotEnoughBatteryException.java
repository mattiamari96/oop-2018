package it.unibo.oop.lab.exception1;

public class NotEnoughBatteryException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final double energyRequired;
	private final double batteryLevel;

	public NotEnoughBatteryException(double energyRequired, double batteryLevel) {
		super();
		this.energyRequired = energyRequired;
		this.batteryLevel = batteryLevel;
	}
	
	public String getMessage() {
		return "Not enough battery. Energy required by action: " + this.energyRequired
				+ " Current battery level: " + this.batteryLevel;
	}
}
