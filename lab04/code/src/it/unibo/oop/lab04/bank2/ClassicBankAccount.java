package it.unibo.oop.lab04.bank2;

public class ClassicBankAccount extends AbstractBankAccount {
	
	public static final double MANAGEMENT_FEE = 5;
	public static final double ATM_TRANSACTION_FEE = 1;
	
	public ClassicBankAccount(int userID, double balance) {
		super(userID, balance);
	}

	public void computeManagementFees(int userID) {
		if (!this.checkUser(userID)) {
			return;
		}
		this.setBalance(this.getBalance() - this.computeFee());
	}

	protected boolean isWithdrawAllowed(double amount) {
		return true;
	}

	protected double getAtmTransactionFee() {
		return ClassicBankAccount.ATM_TRANSACTION_FEE;
	}

	protected double computeFee() {
		return ClassicBankAccount.MANAGEMENT_FEE;
	}
}
