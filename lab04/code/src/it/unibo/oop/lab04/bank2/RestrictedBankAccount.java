package it.unibo.oop.lab04.bank2;

public class RestrictedBankAccount extends AbstractBankAccount {
	
	private static final double ATM_TRANSACTION_FEE = 1;
	private static final double MANAGEMENT_FEE = 5;
	private static final double TRANSACTION_FEE = 0.1;
	    
	public RestrictedBankAccount(int userID, double balance) {
		super(userID, balance);
	}

	public void computeManagementFees(int userID) {
		if (!this.checkUser(userID)) {
			return;
		}
		this.setBalance(this.getBalance() - this.computeFee());
		this.resetTransactions();
	}

	protected boolean isWithdrawAllowed(double amount) {
		return this.getBalance() > amount;
	}

	protected double getAtmTransactionFee() {
		return RestrictedBankAccount.ATM_TRANSACTION_FEE;
	}

	protected double computeFee() {
		return RestrictedBankAccount.MANAGEMENT_FEE
				+ (this.getNTransactions() * RestrictedBankAccount.TRANSACTION_FEE);
	}
}
