package it.unibo.oop.lab04.bank2;

import it.unibo.oop.lab04.bank.BankAccount;

public abstract class AbstractBankAccount implements BankAccount {
	
	private final int userID;
	private double balance;
    private int nTransactions;
	
	public AbstractBankAccount(int userID, double balance) {
		this.userID = userID;
		this.balance = balance;
		this.nTransactions = 0;
	}
	
	public abstract void computeManagementFees(int userID);

	public void deposit(int userID, double amount) {
		this.doTransaction(userID, amount);
	}

	public void depositFromATM(int userID, double amount) {
		this.doTransaction(userID, amount - this.getAtmTransactionFee());
	}

	public double getBalance() {
		return this.balance;
	}

	public int getNTransactions() {
		return this.nTransactions;
	}

	public void withdraw(int userID, double amount) {
		if (!this.isWithdrawAllowed(amount)) {
			return;
		}
		
		this.doTransaction(userID, -amount);
	}

	public void withdrawFromATM(int userID, double amount) {
		double totAmount = amount + this.getAtmTransactionFee();
		
		if (!this.isWithdrawAllowed(totAmount)) {
			return;
		}
		
		this.doTransaction(userID, -totAmount);
	}
	
	protected void setBalance(double balance) {
		this.balance = balance;
	}
	
	protected void doTransaction(int userID, double amount) {
		if (!this.checkUser(userID)) {
			return;
		}
		
		this.balance += amount;
		this.nTransactions++;
	}
	
	protected void resetTransactions() {
        this.nTransactions = 0;
    }
	
	protected boolean checkUser(int id) {
        return this.userID == id;
    }
	
	protected abstract boolean isWithdrawAllowed(double amount);
	protected abstract double getAtmTransactionFee();
	protected abstract double computeFee();
}
