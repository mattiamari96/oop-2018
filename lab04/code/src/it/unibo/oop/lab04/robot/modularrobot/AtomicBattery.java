package it.unibo.oop.lab04.robot.modularrobot;

import it.unibo.oop.lab04.robot.modularrobot.module.AbstractModule;

public class AtomicBattery extends AbstractModule {

	public AtomicBattery() {
	}

	public double getPowerDraw() {
		return 0;
	}

	public void run() {
		if (this.canCharge()) {
			this.robot.recharge();
		}
	}
	
	protected boolean canCharge() {
		return true;
	}

}
