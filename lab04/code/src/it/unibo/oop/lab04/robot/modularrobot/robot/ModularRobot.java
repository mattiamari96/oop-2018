package it.unibo.oop.lab04.robot.modularrobot.robot;

public interface ModularRobot extends BatteryPowered, Movable, Modular {

}
