package it.unibo.oop.lab04.robot.test;

import it.unibo.oop.lab04.robot.arms.BasicArm;
import it.unibo.oop.lab04.robot.modularrobot.BorderNavigator;
import it.unibo.oop.lab04.robot.modularrobot.TestingAtomicBattery;
import it.unibo.oop.lab04.robot.modularrobot.arm.BasicArmAdapter;
import it.unibo.oop.lab04.robot.modularrobot.arm.DropCommand;
import it.unibo.oop.lab04.robot.modularrobot.arm.PickCommand;
import it.unibo.oop.lab04.robot.modularrobot.robot.BaseRobotAdapter;
import it.unibo.oop.lab04.robot.modularrobot.robot.ModularRobot;

/**
 * Utility class for testing composable robots
 * 
 */
public final class TestComposableRobot {

    private static final int CYCLES = 200;

    private TestComposableRobot() {
    }
    
    public static void printTest(String msg, boolean val) {
    	System.out.println(msg + ": " + val);
    }
    
    public static void main(final String[] args) {

        /*
         * Write your own test.
         * 
         * You will need a robot with an atomic battery, two arms, and a
         * navigator system. Turn on the battery only when the level is below
         * 50%.
         */
    	
    	ModularRobot robot = new BaseRobotAdapter("C1P0");
    	
    	robot.addModule(new TestingAtomicBattery());
    	robot.addModule(new BorderNavigator());
    	
    	BasicArmAdapter arm1 = new BasicArmAdapter(new BasicArm("arm_r"));
    	arm1.setCommand(new PickCommand());
    	
    	BasicArmAdapter arm2 = new BasicArmAdapter(new BasicArm("arm_l"));
    	arm2.setCommand(new DropCommand());
    	
    	robot.addModule(arm1);
    	robot.addModule(arm2);
    	
    	robot.powerOnAllModules();
    	
    	for (int i = 0; i < CYCLES; i++) {
    		robot.runAllModules();
    		System.out.println("Battery : " + robot.getBatteryLevel());
    		System.out.println("Position: " + robot.getPosition());
    	}
    	
    }
}
