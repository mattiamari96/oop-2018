package it.unibo.oop.lab04.robot.arms;

public class BasicArm {
	
	public static final double ARM_PICKUP_CONSUMPTION = 0.8;
	public static final double ARM_DROPDOWN_CONSUMPTION = 0.5;
	
	private String name;
	private boolean isGrabbing;
	
	public BasicArm(String name) {
		this.name = name;
	}
	
	public boolean isGrabbing() {
		return this.isGrabbing;
	}
	
	public void pickUp() {
		this.isGrabbing = true;
	}
	
	public void dropDown() {
		this.isGrabbing = false;
	}
	
	public double getConsumptionForPickUp() {
		return BasicArm.ARM_PICKUP_CONSUMPTION;
	}
	
	public double getConsumptionForDropDown() {
		return BasicArm.ARM_DROPDOWN_CONSUMPTION;
	}
	
	public String toString() {
		return "BasicArm [ "
				+ "name=" + this.name + " "
				+ "isGrabbing=" + this.isGrabbing + " "
				+ "]";
	}
}
