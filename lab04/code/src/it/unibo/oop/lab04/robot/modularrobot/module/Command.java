package it.unibo.oop.lab04.robot.modularrobot.module;

public interface Command {
	public String getCommandType();
}
