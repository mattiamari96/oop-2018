package it.unibo.oop.lab04.robot.modularrobot.arm;

import it.unibo.oop.lab04.robot.modularrobot.module.Command;

public interface ArmCommand extends Command {
	public void run(Arm arm);
}
