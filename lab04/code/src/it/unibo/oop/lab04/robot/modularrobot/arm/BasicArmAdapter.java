package it.unibo.oop.lab04.robot.modularrobot.arm;

import it.unibo.oop.lab04.robot.arms.BasicArm;
import it.unibo.oop.lab04.robot.modularrobot.module.AbstractModule;

public class BasicArmAdapter extends AbstractModule implements Arm {
	
	private static final double POWER_DRAW_PICKUP = 0.8;
	private static final double POWER_DRAW_DROPDOWN = 0.5;
	
	private BasicArm arm;
	private ArmCommand command;
	
	public BasicArmAdapter(BasicArm arm) {
		this.arm = arm;
	}

	public boolean isGrabbing() {
		return this.arm.isGrabbing();
	}

	public void pickUp() {
		this.arm.pickUp();
	}

	public void dropDown() {
		this.arm.dropDown();
	}

	public void setCommand(ArmCommand command) {
		this.command = command;
	}

	public double getPowerDraw() {
		if (this.command.getCommandType() == "arm_pick") {
			return POWER_DRAW_PICKUP;
		}
		if (this.command.getCommandType() == "arm_drop") {
			return POWER_DRAW_DROPDOWN;
		}
		
		return 0.0;
	}

	public void run() {
		this.command.run(this);
	}

}
