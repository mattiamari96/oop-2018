package it.unibo.oop.lab04.robot.modularrobot.arm;

public interface Arm {
	public boolean isGrabbing();
	
	public void pickUp();
	public void dropDown();
}
