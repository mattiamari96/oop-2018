package it.unibo.oop.lab04.robot.arms;

import it.unibo.oop.lab04.robot.base.BaseRobot;

public class RobotWithTwoArms extends BaseRobot implements RobotWithArms {
	
	public static final double MOVEMENT_TRANSPORTING_CONSUMPTION = 0.5;
	
	private BasicArm[] arms;
	
	public RobotWithTwoArms(String robotName) {
		super(robotName);
		this.arms = new BasicArm[] {
				new BasicArm("arm_left"),
				new BasicArm("arm_right")
		};
	}

	public boolean pickUp() {
		if (this.getItemsCarried() == 2) {
			return false;
		}
		
		BasicArm arm = this.firstAvailableArm();
		
		if (!isBatteryEnough(arm.getConsumptionForPickUp())) {
			log("Cannot pickup, not enough battery.");
	        return false;
		}
		
        arm.pickUp();
        this.consumeBattery(arm.getConsumptionForPickUp());
		
		return true;
	}

	public boolean dropDown() {
		if (this.getItemsCarried() == 0) {
			return false;
		}
		
		BasicArm arm = this.firstBusyArm();
		
		if (!isBatteryEnough(arm.getConsumptionForDropDown())) {
			log("Cannot dropdown, not enough battery.");
	        return false;
		}
		
        arm.dropDown();
        this.consumeBattery(arm.getConsumptionForDropDown());
		
		return true;
	}

	public int getItemsCarried() {
		int count = 0;
		for (BasicArm arm : this.arms) {
			if (arm.isGrabbing()) {
				count++;
			}
		}
		return count;
	}
	
	protected double getBatteryRequirementForMovement() {
		return super.getBatteryRequirementForMovement()
				+ ( RobotWithTwoArms.MOVEMENT_TRANSPORTING_CONSUMPTION * this.getItemsCarried());
	}
	
	private BasicArm firstAvailableArm() {
		for (BasicArm arm : this.arms) {
			if (!arm.isGrabbing()) {
				return arm;
			}
		}
		
		return null;
	}
	
	private BasicArm firstBusyArm() {
		for (BasicArm arm : this.arms) {
			if (arm.isGrabbing()) {
				return arm;
			}
		}
		
		return null;
	} 
}
