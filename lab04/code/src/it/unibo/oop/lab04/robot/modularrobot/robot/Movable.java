package it.unibo.oop.lab04.robot.modularrobot.robot;

import it.unibo.oop.lab04.robot.base.Position2D;

public interface Movable {
	public boolean moveForward();
	public void rotateClockwise();
	Position2D getPosition();
}
