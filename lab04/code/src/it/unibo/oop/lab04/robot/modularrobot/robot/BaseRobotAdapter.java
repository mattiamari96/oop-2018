package it.unibo.oop.lab04.robot.modularrobot.robot;

import java.util.ArrayList;
import java.util.Collection;

import it.unibo.oop.lab04.robot.base.BaseRobot;
import it.unibo.oop.lab04.robot.modularrobot.module.Module;

public class BaseRobotAdapter extends BaseRobot implements ModularRobot {
	
	private int direction;
	private Collection<Module> modules;
	
	public BaseRobotAdapter(String robotName) {
		super(robotName);
		
		this.direction = 0;
		this.modules = new ArrayList<>();
	}

	public boolean moveForward() {
		switch (this.direction) {
			case 0:	return this.moveUp();
			case 1: return this.moveRight();
			case 2: return this.moveDown();
			case 3: return this.moveLeft();
		}
		
		return false;
	}

	public void rotateClockwise() {
		this.direction += 1;
		if (this.direction > 3) {
			this.direction = 0;
		}
	}

	public boolean addModule(Module module) {
		module.setRobot(this);
		return this.modules.add(module);
	}

	public boolean removeModule(Module module) {
		module.setRobot(null);
		return this.modules.remove(module);
	}
	
	public void powerOnAllModules() {
		for (Module mod : this.modules) {
			mod.powerOn();
		}
	}
	
	public void runAllModules() {
		for (Module mod : this.modules) {
			if (mod.isPoweredOn()) {
				this.consumeBattery(mod.getPowerDraw());
				mod.run();
			}
		}
	}

}
