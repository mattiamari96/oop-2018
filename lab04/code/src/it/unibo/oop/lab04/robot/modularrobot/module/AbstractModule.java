package it.unibo.oop.lab04.robot.modularrobot.module;

import it.unibo.oop.lab04.robot.modularrobot.robot.ModularRobot;

public abstract class AbstractModule implements Module {
	
	protected ModularRobot robot;
	protected boolean powerState;
	
	public AbstractModule() {
		this.powerState = false;
	}

	public void setRobot(ModularRobot robot) {
		this.robot = robot;
	}

	public boolean isPoweredOn() {
		return this.powerState;
	}

	public void powerOn() {
		this.powerState = true;
	}

	public void powerOff() {
		this.powerState = false;
	}

	abstract public double getPowerDraw();

	abstract public void run();

}
