package it.unibo.oop.lab04.robot.modularrobot;

public class TestingAtomicBattery extends AtomicBattery {

	public TestingAtomicBattery() {
		super();
	}
	
	protected boolean canCharge() {
		return this.robot.getBatteryLevel() < 50;
	}
	
}
