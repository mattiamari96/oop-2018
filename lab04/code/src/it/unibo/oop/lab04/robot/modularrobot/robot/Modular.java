package it.unibo.oop.lab04.robot.modularrobot.robot;

import it.unibo.oop.lab04.robot.modularrobot.module.Module;

public interface Modular {
	public boolean addModule(Module module);
	public boolean removeModule(Module module);
	public void runAllModules();
	public void powerOnAllModules();
}
