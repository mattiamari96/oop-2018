package it.unibo.oop.lab04.robot.modularrobot;

import it.unibo.oop.lab04.robot.modularrobot.module.AbstractModule;

public class BorderNavigator extends AbstractModule {
	
	private static final double POWER_DRAW = 0.2;
	
	public BorderNavigator() {
	}

	public double getPowerDraw() {
		return POWER_DRAW;
	}

	public void run() {
		while (!this.robot.moveForward()) {
			this.robot.rotateClockwise();
		}
	}

}
