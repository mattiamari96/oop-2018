package it.unibo.oop.lab04.robot.modularrobot.module;

import it.unibo.oop.lab04.robot.modularrobot.robot.ModularRobot;

public interface Module {
	public void setRobot(ModularRobot robot);
	
	public boolean isPoweredOn();
	public void powerOn();
	public void powerOff();
	public double getPowerDraw();
	
	public void run();
}
