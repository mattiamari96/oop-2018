package it.unibo.oop.lab04.robot.modularrobot.arm;

public class DropCommand implements ArmCommand {

	public DropCommand() {
	}

	public void run(Arm arm) {
		if (arm.isGrabbing()) {
			arm.dropDown();
		}
	}

	public String getCommandType() {
		return "arm_drop";
	}

}
