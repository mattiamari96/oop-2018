package it.unibo.oop.lab04.robot.modularrobot.arm;

public class PickCommand implements ArmCommand {

	public PickCommand() {
	}

	public void run(Arm arm) {
		if (!arm.isGrabbing()) {
			arm.pickUp();
		}
	}

	public String getCommandType() {
		return "arm_pick";
	}

}
