package it.unibo.oop.lab04.robot.modularrobot.robot;

public interface BatteryPowered {
	public void recharge();
	public double getBatteryLevel();
}
