package it.unibo.oop.lab04.bank;

public class ExtendedStrictBankAccount extends SimpleBankAccount {

	public ExtendedStrictBankAccount(int usrID, double balance) {
		super(usrID, balance);
	}
	
	public void computeManagementFees(final int usrID) {
        if (checkUser(usrID)) {
        	double balance = super.getBalance();
            super.setBalance(balance - (0.1 * super.getNTransactions()));
        }
    }
	
	public void withdraw(final int usrID, final double amount) {
		if (this.getBalance() < amount) {
			return;
		}
        super.withdraw(usrID, -amount);
    }
}
