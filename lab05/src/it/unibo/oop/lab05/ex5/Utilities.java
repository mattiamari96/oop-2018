package it.unibo.oop.lab05.ex5;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Random;

/**
 *
 */
public final class Utilities {

    private Utilities() {
    }

    /**
     * Copies all the element of the first collection in the second collection.
     * 
     * @param source
     *            the source collection
     * @param target
     *            the target collection
     * @param <X>
     *            Collection type
     */
    public static <X> void copyAll(final Collection<X> source, final Collection<? super X> target) {
    	for (X elem : source) {
    		target.add(elem);
    	}
    }

    /**
     * @param coll
     *            the collection
     * @param <X>
     *            collection type
     * @return a random element from the collection
     * 
     */
    public static <X> X getRandomElement(final Collection<X> coll) {
    	Random rnd = new Random();
        Iterator<X> iter = coll.iterator();
        int idx = (int)rnd.nextInt(coll.size());
        while (idx > 0) {
        	iter.next();
        	idx--;
        }
        
        return iter.next();
    }

    /**
     * @param one
     *            first collection
     * @param two
     *            second collection
     * @param <X>
     *            First collection type
     * @param <Y>
     *            Second collection type
     * @return a pair with two random elements
     */
    public static <X, Y> Pair<X, Y> getRandomPair(final Collection<X> one, final Collection<Y> two) {
    	Random rnd = new Random();
        Iterator<X> iter1 = one.iterator();
        Iterator<Y> iter2 = two.iterator();
        
        int idx = rnd.nextInt(one.size());
        while (idx > 0) {
        	iter1.next();
        	idx--;
        }
        
        idx = rnd.nextInt(two.size());
        while (idx > 0) {
        	iter2.next();
        	idx--;
        }
        
        return new Pair<X, Y>(iter1.next(), iter2.next());
    }
}
