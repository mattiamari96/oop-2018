package it.unibo.oop.lab05.ex4;

import it.unibo.oop.lab05.ex3.Product;
import it.unibo.oop.lab05.ex3.ProductImpl;

public class ComparableProduct extends ProductImpl implements Comparable<Product> {

	public ComparableProduct(String name) {
		super(name);
	}

	public int compareTo(Product prod) {
		return this.getName().compareTo(prod.getName());
	}

}
