package it.unibo.oop.lab05.ex3;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public class WarehouseImpl implements Warehouse {

	private Set<Product> products;
	
	public WarehouseImpl() {
		this.products = new LinkedHashSet<>();
	}

	@Override
	public void addProduct(Product p) {
		this.products.add(p);
	}

	@Override
	public Set<String> allNames() {
		Set<String> names = new HashSet<>();
		
		for (Product prod : this.products) {
			names.add(prod.getName());
		}
		
		return names;
	}

	@Override
	public Set<Product> allProducts() {
		return new LinkedHashSet<Product>(this.products);
	}

	@Override
	public boolean containsProduct(Product p) {
		return this.products.contains(p);
	}

	@Override
	public double getQuantity(String name) {
		for (Product prod : this.products) {
			if (prod.getName() == name) {
				return prod.getQuantity();
			}
		}
		
		return -1;
	}

}
