package it.unibo.oop.lab05.ex3;

public class ProductImpl implements Product {
	
	private String name;
	private int quantity;
	
	public ProductImpl(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public double getQuantity() {
		return this.quantity;
	}
	
	public Product setQuantity(int quantity) {
		this.quantity = quantity;
		return this;
	}
	
	public boolean equals(Product prod) {
		return this.name == prod.getName();
	}
	
	public int hashCode() {
		return this.name.hashCode();
	}
	
	public String toString() {
		return "Product [ "
				+ this.name + " "
				+ this.quantity + " ]";
	}

}
