
# Laboratorio 01

* Si completino gli esercizi in ordine
* All'interno di ogni cartella, è presente un file `README.md` contenente le istruzioni dell'esercizio.
* Si leggano le istruzioni *con molta attenzione*
* Si cerchi di procedere autonomamente, contattando i docenti in caso di necessità
* Al termine di ciascun esercizio, si contattino i docenti per mostrare quanto svolto

### Note
* Non modificare, quando esplicitamente forniti, i nomi dei metodi, né i loro tipi in ingresso o di ritorno
* È consigliabile, qualora lo si ritenga necessario, creare dei metodi d'appoggio per semplificare la scrittura e/o la risoluzione degli esercizi
* È possibile fare riferimento alle funzionalità fornite dalle seguenti classi:
  - [java.util.Arrays](https://docs.oracle.com/javase/8/docs/api/java/util/Arrays.html)
  - [java.lang.Math](https://docs.oracle.com/javase/8/docs/api/java/lang/Math.html)

**IMPORTANTE**: non si attenda di aver completato più esercizi per chiamare il docente, lo si chiami appena terminato l'esercizio, in modo da evitare di arrivare a fine laboratorio con molti esercizi non corretti. Una volta chiamato il docente, mentre se ne attende l'arrivo, si continui con l'esercizio successivo.
