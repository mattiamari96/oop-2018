package oop.lab02.arrays;

public class MyCircularArray {

    public static final int ARRAY_DEF_SIZE = 10;
    int[] array;
	int idx;
	
	public MyCircularArray() {
		this(ARRAY_DEF_SIZE);
	}
	
	public MyCircularArray(int size) {
		this.array = new int[size];
		this.idx = 0;
	}
	
    public void add(final int elem) {
		if (this.idx == this.array.length) {
			this.idx = 0;
		}
		
		this.array[this.idx] = elem;
		this.idx += 1;
    }

    public void reset() {
		for (int i = 0; i < this.array.length; i++) {
			this.array[i] = 0;
		}
    }

    public void printArray() {
        System.out.print("[");
        for (int i = 0; i < this.array.length - 1; i++) {
            System.out.print(this.array[i] + ",");
        }
        System.out.println(this.array[this.array.length - 1] + "]");
    }

    public static void main(final String[] args) {
        /*
         * 1) Creare un array circolare di dieci elementi
         * 
         * 2) Aggiungere gli elementi da 1 a 10 e stampare il contenuto
         * dell'array circolare
         * 
         * 3) Aggiungere gli elementi da 11 a 15 e stampare il contenuto
         * dell'array circolare
         * 
         * 4) Invocare il metodo reset
         * 
         * 5) Stampare il contenuto dell'array circolare
         * 
         * 6) Aggiungere altri elementi a piacere e stampare il contenuto
         * dell'array circolare
         */
		 
		MyCircularArray arr = new MyCircularArray(10);
		
		for (int i = 1; i <= 10; i++) {
			arr.add(i);
		}
		arr.printArray();
		
		for (int i = 11; i <= 15; i++) {
			arr.add(i);
		}
		arr.printArray();
		
		arr.reset();
		arr.printArray();
		
		for (int i = 20; i <= 35; i++) {
			arr.add(i);
		}
		arr.printArray();
    }
}
