package oop.lab02.arrays;

import oop.lab02.constructors.Smartphone;

public class PlayWithSmartphoneArrays {

    static int search(final Smartphone[] array, final String brand) {
        int count = 0;
		for (Smartphone sp : array) {
			if (sp.brand == brand) {
				count += 1;
			}
		}
		return count;
    }

    static int search(final Smartphone[] array, final boolean hasNFC, final boolean hasGPS) {
        int count = 0;
		for (Smartphone sp : array) {
			if (sp.hasNFC == hasNFC && sp.hasGPS == hasGPS) {
				count += 1;
			}
		}
		return count;
    }

    static int search(final Smartphone[] array, final int nCPUs, final int sdSize, final boolean has3G) {
        int count = 0;
		for (Smartphone sp : array) {
			if (sp.nCPU == nCPUs && sp.sdSize == sdSize && sp.has3G == has3G) {
				count += 1;
			}
		}
		return count;
    }
	
	static boolean testSearch(Smartphone[] array) {
		return search(array, "HTC") == 1
			&& search(array, "pippo") == 0
			&& search(array, "Acer") == 2
			&& search(array, false, false) == 0
			&& search(array, true, true) == 3
			&& search(array, 2, 8192, false) == 0;
	}
	
    public static void main(final String[] args) {
        /*
         * 1) Creare lo smarthpone HTC One ram:1024 sdSize
         *
         * 2) Creare lo smarthpone Samsung Galaxy Note 3 ram:2048 sdSize:8192
         * gps:true nfc:true 3g:true
         *
         * 3) Creare lo smarthpone iPhone 5S nfc:false
         *
         * 4) Creare lo smarthpone Google Nexus 4 gps:true 3g:true
         *
         * 5) Creare lo smarthpone Acer Liquid cpu:2 ram:512 sdSize:8192 gps:
         * true 3g:true nfc: false
         *
         * 6) Eseuguire diverse operazioni di ricerca con i metodi search
         * implementati e controllare la corrispondenza del numero dei telefoni
         * facenti match
         */

        Smartphone[] sms = {
            new Smartphone("HTC", "One", 8192, Smartphone.DEF_N_CPU, 1024, true, true, true),
            new Smartphone(
                "Samsung",
                "Galaxy Note 3",
                8192,
                Smartphone.DEF_N_CPU,
                2048,
                true,
                true,
                true
            ),
            new Smartphone("Apple", "iPhone 5Sh*t", false),
            new Smartphone(
                "Google",
                "Nexus",
                Smartphone.DEF_SD_SIZE,
                Smartphone.DEF_N_CPU,
                Smartphone.DEF_RAM_SIZE,
                true,
                true,
                Smartphone.DEF_HAS_NFC
            ),
            new Smartphone(
                "Acer",
                "Liquid",
                8192,
                2,
                512,
                true,
                true,
                false
            ),
			new Smartphone(
                "Acer",
                "Qualcosa",
                8192,
                2,
                512,
                true,
                true,
                false
            )
        };
		
		/*for (Smartphone sp : sms) {
			sp.printStringRep();
			System.out.println("");
		}*/
		
		System.out.println("testSearch: " + testSearch(sms));
    }
}
