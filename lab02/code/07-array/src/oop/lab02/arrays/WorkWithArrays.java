package oop.lab02.arrays;

import java.util.Arrays;

public class WorkWithArrays {

    /*
    * ADVICE:
    * If your method implements an algorithm, it doesn't necessarly need to contain
    * the whole code! You can decomopose your problem into simpler (non-trivial) ones,
    * writing a *private* utility method for each sub problem.
    * Moreover, if your utility method solves a common sub-problem, it may be
    * re-used by other methods within the same class.
    */


    public static int countOccurrencies(final int[] array, final int elem) {
        int nOccur = 0;

        for (final int currElem : array) {
            if (currElem == elem) {
                nOccur++;
            }
        }

        return nOccur;
    }

    public static int[] evenElements(final int[] array) {
		int out[] = new int[evenArrayLength(array)];

        for (int i = 0; i < array.length; i+=2) {
			out[i / 2] = array[i];
		}

		return out;
    }

    public static int[] oddElements(final int[] array) {
		int out[] = new int[oddArrayLength(array)];

        for (int i = 1; i < array.length; i+=2) {
			out[(i - 1) / 2] = array[i];
		}

		return out;
    }

    public static int mostRecurringElement(final int[] array) {
        int[] counts = new int[arrayMax(array) + 1];

        for (int n : array) {
            counts[n] += 1;
        }

        return arrayMaxPos(counts);
    }

    public static int[] revertUpTo(int[] array, int stopElement) {
        int[] out = new int[array.length];
        int stopPos = arrayFind(array, stopElement);

        for (int i = stopPos; i >= 0; i--) {
            out[stopPos - i] = array[i];
        }
        for (int i = stopPos + 1; i < array.length; i++) {
            out[i] = array[i];
        }

        return out;
    }
	
	public static int[] sortArray(int[] array, boolean desc) {
		int[] out = new int[array.length];
		
		for (int i = 0; i < array.length; i++) {
			out[i] = array[i];
		}
		
		int tmp;
		for (int i = 0; i < out.length; i++) {
			for (int j = i + 1; j < out.length; j++) {
				if ((desc && out[i] < out[j]) || (!desc && out[i] > out[j])) {
					tmp = out[i];
					out[i] = out[j];
					out[j] = tmp;
				}
			}
		}
		
		return out;
	}
	
	public static double computeVariance(double[] array) {
		double var = 0;
		double mean = arrayMean(array);
		
		for(int n = 0; n < array.length; n++)
		{
		  var += (array[n] - mean) * (array[n] - mean);
		}
		
		var /= array.length;
		return Math.sqrt(var);
	}
	
	private static double arrayMean(double[] array) {
		double sum = 0;
		
		for (double el : array) {
			sum += el;
		}
		
		return sum / array.length;
	}

    private static int arrayMax(final int[] array) {
        int max = array[0];

        for (int n : array) {
            if (n > max) {
                max = n;
            }
        }

        return max;
    }

    private static int arrayMaxPos(final int[] array) {
        int max = arrayMax(array);

        for (int i = 0; i < array.length; i++) {
            if (array[i] == max) {
                return i;
            }
        }

        return -1;
    }

    private static int arrayFind(int[] array, int needle) {
        int i = 0;
        for (; i < array.length && array[i] != needle; i++);
        if (i < array.length) return i;
        return -1;
    }

    private static int evenArrayLength(final int[] array) {
        if (array.length % 2 == 0) {
            return array.length / 2;
        }

        return (array.length / 2) + 1;
    }

    private static int oddArrayLength(final int[] array) {
        return array.length / 2;
    }

    /*
     * Testing methods
     */

    public static boolean testCountOccurrencies() {
        return countOccurrencies(new int[] { 1, 2, 3, 4 }, 1) == 1
                && countOccurrencies(new int[] { 0, 2, 3, 4 }, 1) == 0
                && countOccurrencies(new int[] { 7, 4, 1, 9, 3, 1, 5 }, 2) == 0
                && countOccurrencies(new int[] { 1, 2, 1, 3, 4, 1 }, 1) == 3
				&& countOccurrencies(new int[] { 2, 2, 2, 3, 2, 2 }, 2) == 5;
    }

    public static boolean testEvenElements() {
        return Arrays.equals(evenElements(new int[] { 1, 2, 3, 4 }), new int[] { 1, 3 })
                && Arrays.equals(
                        evenElements(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 }),
                        new int[] { 1, 3, 5, 7, 9 })
                && Arrays.equals(
                        evenElements(new int[] { 4, 6, 7, 9, 1, 5, 23, 11, 73 }),
                        new int[] { 4, 7, 1, 23, 73 })
                && Arrays.equals(
                        evenElements(new int[] { 7, 5, 1, 24, 12, 46, 23, 11, 54, 81 }),
                        new int[] { 7, 1, 12, 23, 54 });
    }

    public static boolean testOddElements() {
        return Arrays.equals(oddElements(new int[] { 1, 2, 3, 4 }),
                new int[] { 2, 4 })
                && Arrays.equals(
                        oddElements(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 }),
                        new int[] { 2, 4, 6, 8 })
                && Arrays.equals(
                        oddElements(new int[] { 4, 6, 7, 9, 1, 5, 23, 11, 73 }),
                        new int[] { 6, 9, 5, 11 })
                && Arrays.equals(
                        oddElements(new int[] { 7, 5, 1, 24, 12, 46, 23, 11, 54, 81 }),
                        new int[] { 5, 24, 46, 11, 81 });
    }

    public static boolean testMostRecurringElement() {
        return mostRecurringElement(new int[] { 1, 2, 1, 3, 4 }) == 1
                && mostRecurringElement(new int[] { 7, 1, 5, 7, 7, 9 }) == 7
                && mostRecurringElement(new int[] { 1, 2, 3, 1, 2, 3, 3 }) == 3
                && mostRecurringElement(new int[] { 5, 11, 2, 11, 7, 11 }) == 11;
    }

    public static boolean testRevertUpTo() {
        return Arrays.equals(revertUpTo(new int[] { 1, 2, 3, 4 }, 1), new int[] { 1, 2, 3, 4 })
                && Arrays.equals(
                        revertUpTo(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 }, 6),
                        new int[] { 6, 5, 4, 3, 2, 1, 7, 8, 9 })
                && Arrays.equals(
                        revertUpTo(new int[] { 4, 6, 7, 9, 1, 5, 23, 11, 73 }, 1),
                        new int[] { 1, 9, 7, 6, 4, 5, 23, 11, 73 })
                && Arrays.equals(
                        revertUpTo(new int[] { 7, 5, 1, 24, 12, 46, 23, 11, 54, 81 }, 81),
                        new int[] { 81, 54, 11, 23, 46, 12, 24, 1, 5, 7 });
    }
	
	public static boolean testSortArray() {
		return Arrays.equals(sortArray(new int[] {3, 1, 4, 2}, false), new int[] {1, 2, 3, 4})
			&& Arrays.equals(sortArray(new int[] {3, 1, 4, 2}, true), new int[] {4, 3, 2, 1});
	}
	
	public static boolean testComputeVariance() {
			return computeVariance(new double[] {1, 1, 1}) == 0;
	}

    public static void main(final String[] args) {
        System.out.println("testCountOccurr: " + testCountOccurrencies());
        System.out.println("testEvenElems: " + testEvenElements());
        System.out.println("testOddElems: " + testOddElements());
        System.out.println("testGetMostRecurringElem: " + testMostRecurringElement());
        System.out.println("testRevertUpTo: " + testRevertUpTo());
		System.out.println("testSortArray: " + testSortArray());
		System.out.println("testComputeVariance: " + testComputeVariance());
    }
}
