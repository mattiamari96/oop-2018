class ComplexNumCalculator {
    int numOp;
    ComplexNum lastRes;
    
    void build() {
        this.numOp = 0;
        ComplexNum last = new ComplexNum();
        last.build(0, 0);
        this.lastRes = last;
    }
    
    ComplexNum add(ComplexNum a, ComplexNum b) {
        ComplexNum res = new ComplexNum();
        res.build(a.re + b.re, a.im + b.im);
        this.numOp += 1;
        this.lastRes = res;
        return res;
    }
    
    ComplexNum sub(ComplexNum a, ComplexNum b) {
        ComplexNum res = new ComplexNum();
        res.build(a.re - b.re, a.im - b.im);
        this.numOp += 1;
        this.lastRes = res;
        return res;
    }
    
    ComplexNum mul(ComplexNum a, ComplexNum b) {
        ComplexNum res = new ComplexNum();
        res.build(
            a.re * b.re - a.im * b.im,
            a.im * b.re + a.re * b.im
        );
        this.numOp += 1;
        this.lastRes = res;
        return res;
    }
    
    ComplexNum div(ComplexNum a, ComplexNum b) {
        ComplexNum res = new ComplexNum();
        
        double x = a.re * b.re + a.im * b.im;
        double y = a.im * b.re - a.re * b.im;
        double z = b.re * b.re + b.im * b.im;
        
        res.build(
            x / z,
            y / z
        );
        this.numOp += 1;
        this.lastRes = res;
        return res;
    }
}