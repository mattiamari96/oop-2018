class Calculator {
    
    int numOp;
    double lastRes;
    
    void build() {
        this.numOp = 0;
        this.lastRes = 0;
    }
    
    double add(double a, double b) {
        this.numOp += 1;
        this.lastRes =  a + b;
        return this.lastRes;
    }
    
    double sub(double a, double b) {
        this.numOp += 1;
        this.lastRes =  a - b;
        return this.lastRes;
    }
    
    double mul(double a, double b) {
        this.numOp += 1;
        this.lastRes =  a * b;
        return this.lastRes;
    }
    
    double div(double a, double b) {
        this.numOp += 1;
        this.lastRes =  a / b;
        return this.lastRes;
    }
}