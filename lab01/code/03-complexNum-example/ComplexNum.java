class ComplexNum {

    /*
     * Inserire qui la dichiarazione dei campi della classe
     */
    
    double re;
    double im;
    
    void build(double re, double im) {
        this.re = re;
        this.im = im;
    }

    boolean equal(ComplexNum num) {
        return this.re == num.re && this.im == num.im;
    }

    void add(ComplexNum num) {
        this.re += num.re;
        this.im += num.im;
    }

    String toStringRep() {
        return this.re + "+" + this.im + "i";
    }
}
