class TestStudent {
  public static void main(String[] args) {
      Student student = new Student();
      /*
       * aggiornare l'invocazione del metodo build con l'aggiunta dei
       * parametri
       */
      
      /*
       * Creare dei nuovi oggetti relativi agli studenti:
       *
       * - Nome: Luigi Cognome: Gentile id: 1015, matriculationYear: 2012 -
       * Nome: Simone Cognome: Bianchi id: 1016, matriculationYear: 2010
       *
       * - Nome: Andrea Cognome: Bracci id: 1017, matriculationYear: 2012
       *
       * Stampare a video le informazioni relative a ciascuno studente.
       */
       
        student.build("Luigi", "Gentile", 1015, 2012);
        student.printStudentInfo();
        
        Student st2 = new Student();
        st2.build("Simone", "Bianchi", 1016, 2010);
        st2.printStudentInfo();
        
        Student st3 = new Student();
        st3.build("Andrea", "Bracci", 1017, 2012);
        st3.printStudentInfo();
  }
}
