class Recognizer {
    
    int nextVal;
    
    public Recognizer() {
        this.reset();
    }
    
    int nextExpectedInt() {
        return nextVal;
    }
    
    void reset() {
        this.nextVal = 1;
    }
    
    boolean check1(int num) {
        if (num == 1) {
            this.nextVal = 2;
            return true;
        }
        return false;
    }
    
    boolean check2(int num) {
        if (num == 2) {
            this.nextVal = 3;
            return true;
        }
        return false;
    }
    
    boolean check3(int num) {
        if (num == 3) {
            this.nextVal = 4;
            return true;
        }
        return false;
    }
    
    boolean check4(int num) {
        if (num == 4) {
            this.nextVal = 1;
            return true;
        }
        return false;
    }
}