
# Laboratorio 01

## Scelta dell'editor di testo

Il laboratorio è equipaggiato con diversi editor di testo. Per svolgere l'esercizio (ed i seguenti) sarà necessario l'uso di uno dei seguenti strumenti:

* Jedit
* Notepad++
* Atom

Se avete la vostra macchina personale, esistono altri ambienti per la modifica di file di testo altrettanto validi, fra cui:

* Gedit
* Kate
* Kwrite
* VisualStudio Code

Se si preferiscono ambienti di editing di testo su terminale (per chi è **già in grado di usarli**) è possibile usare software come `vim`, `nano`, od `emacs`.

**NON** sono adatti per l'apertura e la modifica di file Java (né di alcun altro linguaggio di programmazione) i word processors (Libreoffice Writer, Openoffice Writer, Microsoft Word, Microsoft WordPad...), né l'editor di testo incluso in Windows (Notepad).

Se indecisi, raccomandiamo l'uso di **JEdit**, che è un client scritto in Java, multipiattaforma, supporta sia Java che altri linguaggi tramite un'architettura a plugin.

## Svolgimento dell'esercizio

Si tengano a portata di consultazione le slide del modulo 02 usato a lezione.

1. Con l'editor di testo scelto (e.g. JEdit) si apra il file `HelloWorld.java`.
2. Si studi il comportamento della classe. Ci si assicuri di aver compreso cosa la classe fa prima di procedere ai punti successivi.
3. Si posizioni un terminale all'interno della cartella utilizzando in modo opportuno il comando `cd`
4. Si compili il sorgente utilizzando il comando `javac HelloWorld.java`
5. Si esegua il programma utilizzando il comando `java HelloWorld`
6. Si prepari la risposta alla seguente domanda: "perché nel comando `javac` è stato messo il nome del file con l'estensione, mentre nel comando `java` non ce n'è stato bisogno?"
7. Si sostituisca la stampa a video corrente con un messaggio diverso a piacere
8. Si ricompili il sorgente
9. Si esegua il programma e se ne verifichi il corretto funzionamento
10. Si aggiunga al messaggio modificato anche la stampa della computazione di 50 + 50 * 50. *Nota*: la computazione deve essere svolta dall'interprete Java!
  - Nota: l'esercizio deve essere risolto in una sola riga di codice: non si possono dichiarare variabili, e  tutte le operazioni vanno svolte *internamente* alla chiamata a `println`
11. Si compili e si esegua, verificando il funzionamento
