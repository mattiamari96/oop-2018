# Laboratorio 01

Nota importante: in tutti gli esercizi che seguono, a partire da questo, si prega di *non modificare*, se forniti dai docenti:

* I nomi dei metodi
* Il numero ed il tipo di parametri in ingresso ai metodi
* Il tipo di ritorno dei metodi

## Svolgimento dell'esercizio

1. Analizzare il sorgente `TestScopes.java`
2. Prima di compilare e lanciare il programma riflettere sul comportamento dei metodi della classe e provare a prevedere l'output di ogni singola stampa, scrivendo su un foglio il risultato previsto
3. Si compili programma *Nota*: Essendo presenti due file, il compilatore `javac` andrà invocato due volte, una per ciascun file. Alternativamente, è possibile invocare `javac` passando entrambi i file come parametro, oppure utilizzare la wildcard `*` seguita dall'estensione del file per compilare tutti i file in una sola passata. Si provi a compilare usando tutte e tre le metodologie descritte. Fra una compilazione e la successiva si eliminino i file con estensione `.class` generati dal compilatore
4. Si esegua il programma, e se ne confronti l'output con quanto previsto al punto 2. Se l'output è differente, si cerchi di capirne il motivo, quindi si chiami il docente per spiegarglielo (o per chiederglielo, se la ragione non fosse chiara).
