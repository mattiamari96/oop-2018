class Train {
    int nTotSeats;
    int nFirstClassSeats;
    int nSecondClassSeats;
    int nFirstClassReservedSeats;
    int nSecondClassReservedSeats;

    void build(int nFirstClassSeats, int nSecondClassSeats) {
        this.nFirstClassSeats = nFirstClassSeats;
        this.nSecondClassSeats = nSecondClassSeats;
        this.nTotSeats = this.nFirstClassSeats + this.nSecondClassSeats;
        this.nFirstClassReservedSeats = 0;
        this.nSecondClassReservedSeats = 0;
    }
    
    void reserveFirstClassSeats(int amount) {
        this.nFirstClassReservedSeats += amount;
    }
    
    void reserveSecondClassSeats(int amount) {
        this.nSecondClassReservedSeats += amount;
    }
    
    double getTotOccupancyRatio() {
        return (this.nFirstClassReservedSeats + this.nSecondClassReservedSeats)
            / (double)this.nTotSeats * 100;
    }
    
    double getFirstClassOccupancyRatio() {
        return this.nFirstClassReservedSeats / (double)this.nFirstClassSeats * 100;
    }
    
    double getSecondClassOccupancyRatio() {
        return this.nSecondClassReservedSeats / (double)this.nSecondClassSeats * 100;
    }
    
    void deleteAllReservations() {
        this.nFirstClassReservedSeats = 0;
        this.nSecondClassReservedSeats = 0;
    }
}