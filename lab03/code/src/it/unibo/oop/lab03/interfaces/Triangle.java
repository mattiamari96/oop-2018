package it.unibo.oop.lab03.interfaces;

public class Triangle implements Polygon {
	private double edgeA;
	private double edgeB;
	private double edgeC;
	
	public Triangle(double edgeA, double edgeB, double edgeC) {
		this.edgeA = edgeA;
		this.edgeB = edgeB;
		this.edgeC = edgeC;
	}
	
	public double getArea() {
	    double s = (this.edgeA + this.edgeB + this.edgeC) / 2;
	    return Math.sqrt(s * (s - this.edgeA) * (s - this.edgeB) * (s - this.edgeC));
	}

	public double getPerimeter() {
		return this.edgeA + this.edgeB + this.edgeC;
	}

	public int getEdgeCount() {
		return 3;
	}

}
