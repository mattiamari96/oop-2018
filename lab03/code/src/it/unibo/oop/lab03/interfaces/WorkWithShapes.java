package it.unibo.oop.lab03.interfaces;

public class WorkWithShapes {

	public static void main(String[] args) {
		Shape s[] = {
				new Circle(5),
				new Square(10),
				new Rectangle(10, 5),
				new Triangle(10, 15, 20)
		};
		
		for (Shape sh : s) {
			System.out.println("[ " + sh.getClass().getName() + " ]");
			System.out.println("     Area: " + sh.getArea());
			System.out.println("Perimeter: " + sh.getPerimeter() + "\n");
		}
	}

}
