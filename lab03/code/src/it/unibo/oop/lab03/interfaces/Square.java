package it.unibo.oop.lab03.interfaces;

public class Square implements Polygon {
	private double edge;
	
	public Square(double edgeLength) {
		this.edge = edgeLength;
	}
	
	public double getArea() {
		return this.edge * this.edge;
	}

	public double getPerimeter() {
		return 4 * this.edge;
	}

	public int getEdgeCount() {
		return 4;
	}

}
