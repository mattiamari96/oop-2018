package it.unibo.oop.lab03.interfaces;

public interface Shape {
	public double getArea();
	public double getPerimeter();
}
