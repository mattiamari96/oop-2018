package it.unibo.oop.lab03.interfaces;

public class StrictBankAccount implements BankAccount {
	private static double ATM_TRANSACTION_FEE = 1;
	
	private int userID;
    private double balance;
    private int nTransactions;

    
    public StrictBankAccount(int userID, double initialAmount) {
    	this.userID = userID;
    	this.balance = initialAmount;
    }

    
    public int getUserID() {
		return this.userID;
	}

	public double getBalance() {
		return this.balance;
	}

	public int getNTransactions() {
		return this.nTransactions;
	}

	public void deposit(final int usrID, final double amount) {
		if (!this.checkUser(usrID)) {
			return;
		}
		
		this.balance += amount;
		this.nTransactions += 1;
    }

    public void withdraw(final int usrID, final double amount) {
    	if (!this.checkUser(usrID) || amount > this.balance) {
			return;
		}
		
		this.balance -= amount;
		this.nTransactions += 1;
    }

    public void depositFromATM(final int usrID, final double amount) {
    	if (!this.checkUser(usrID)) {
			return;
		}
		
		this.balance += amount - this.ATM_TRANSACTION_FEE;
		this.nTransactions += 1;
    }

    public void withdrawFromATM(final int usrID, final double amount) {
    	if (!this.checkUser(usrID) || amount + this.ATM_TRANSACTION_FEE > this.balance) {
			return;
		}
		
		this.balance -= amount + this.ATM_TRANSACTION_FEE;
		this.nTransactions += 1;
    }
    
    public void computeManagementFees(int usrID) {
		this.balance -= 5.0 + (0.1 * this.nTransactions);
	}

    /* Utility method per controllare lo user */
    private boolean checkUser(final int id) {
        return this.userID == id;
    }
}
