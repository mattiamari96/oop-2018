package it.unibo.oop.lab03.interfaces;

public class Rectangle implements Polygon {
	private double shortEdge;
	private double longEdge;
	
	public Rectangle(double shortEdge, double longEdge) {
		this.shortEdge = shortEdge;
		this.longEdge = longEdge;
	}
	
	public double getArea() {
		return this.shortEdge * this.longEdge;
	}

	public double getPerimeter() {
		return 2 * (this.shortEdge + this.longEdge);
	}

	public int getEdgeCount() {
		return 4;
	}

}
