package it.unibo.oop.lab03.interfaces;

import it.unibo.oop.lab03.encapsulation.AccountHolder;
import it.unibo.oop.lab03.encapsulation.SimpleBankAccount;

public final class TestBankAccount {

    private TestBankAccount() { }

    private static void printRes(String desc, boolean val) {
    	System.out.println(desc + ": " + val);
    }
    
    public static void main(final String[] args) {
        /*
         * 1) Creare l' AccountHolder relativo a Mario Rossi con id 1 2) Creare
         * l' AccountHolder relativo a Luigi Bianchi con id 2 3) Dichiarare due
         * variabili (acc1 e acc2) di tipo BankAccount 4) Creare in acc1 un
         * nuovo oggetto di tipo SimpleBankAccount relativo al conto di Mario Rossi
         * (ammontare iniziale = 0) 5) Creare in acc2 un nuovo oggetto di tipo
         * StrictBankAccount relativo al conto di Luigi Bianchi (ammontare
         * iniziale = 0) 6) Prima riflessione: perchè è stato possibile fare la
         * new di due classi diverse in variabili dello stesso tipo? 7)
         * Depositare 10000$ in entrambi i conti 8) Prelevare 15000$ in entrambi
         * i conti 9) Stampare in stdout l'ammontare corrente 10) Qual'è il
         * risultato e perchè? 11) Depositare nuovamente 10000$ in entrambi i
         * conti 11) Invocare il metodo computeManagementFees su entrambi i
         * conti 12) Stampare a video l'ammontare corrente 13) Qual'è il
         * risultato e perchè?
         */
    	
    	AccountHolder ah = new AccountHolder("Mario", "Rossi", 1);
    	BankAccount ba = new StrictBankAccount(1, 1000);
    	
    	printRes("initial amount", ba.getBalance() == 1000 && ba.getNTransactions() == 0);
    	
    	ba.deposit(1, 100);
    	printRes("deposit", ba.getBalance() == 1100 && ba.getNTransactions() == 1);
    	
    	ba.depositFromATM(1, 100);
    	printRes("depositFromATM", ba.getBalance() == 1199 && ba.getNTransactions() == 2);
    	
    	ba.withdraw(1, 100);
    	printRes("withdraw", ba.getBalance() == 1099 && ba.getNTransactions() == 3);
    	
    	ba.withdrawFromATM(1, 90);
    	printRes("withdrawFromATM", ba.getBalance() == 1008 && ba.getNTransactions() == 4);
    	
    	ba.deposit(2, 10);
    	printRes("deposit wrong id", ba.getBalance() == 1008 && ba.getNTransactions() == 4);
    	
    	ba.depositFromATM(2, 10);
    	printRes("deposit wrong id", ba.getBalance() == 1008 && ba.getNTransactions() == 4);
    	
    	ba.withdraw(2, 10);
    	printRes("withdraw wrong id", ba.getBalance() == 1008 && ba.getNTransactions() == 4);
    	
    	ba.withdrawFromATM(2, 10);
    	printRes("withdrawFromATM wrong id", ba.getBalance() == 1008 && ba.getNTransactions() == 4);
    	
    	ba.withdraw(1, 1009);
    	printRes("withdraw too big amount", ba.getBalance() == 1008 && ba.getNTransactions() == 4);
    	
    	ba.withdrawFromATM(1, 1008);
    	printRes("withdrawFromATM too big amount", ba.getBalance() == 1008 && ba.getNTransactions() == 4);
    	
    	ba.computeManagementFees(1);
    	printRes("computeManagementFees", ba.getBalance() == 1008 - 5 - (0.1 * 4));
    }
}
