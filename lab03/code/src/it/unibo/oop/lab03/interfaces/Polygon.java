package it.unibo.oop.lab03.interfaces;

public interface Polygon extends Shape {
	public int getEdgeCount();
}
