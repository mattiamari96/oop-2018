package it.unibo.oop.lab03.encapsulation;

public final class TestSimpleBankAccount {

    private TestSimpleBankAccount() { }
    
    private static void printRes(String desc, boolean val) {
    	System.out.println(desc + ": " + val);
    }
    
    public static void main(final String[] args) {
        /*
         * 1) Creare l' AccountHolder relativo a Mario Rossi con id 1 2) Creare
         * l' AccountHolder relativo a Luigi Bianchi con id 2 3) Creare i due
         * SimpleBankAccount corrispondenti 4) Effettuare una serie di depositi e
         * prelievi 5) Stampare a video l'ammontare dei due conti e verificare
         * la correttezza del risultato 6) Provare a prelevare fornendo un idUsr
         * sbagliato 7) Controllare nuovamente l'ammontare
         */
    	
    	AccountHolder ah = new AccountHolder("Mario", "Rossi", 1);
    	SimpleBankAccount ba = new SimpleBankAccount(1, 1000);
    	
    	printRes("initial amount", ba.getBalance() == 1000 && ba.getNTransactions() == 0);
    	
    	ba.deposit(1, 100);
    	printRes("deposit", ba.getBalance() == 1100 && ba.getNTransactions() == 1);
    	
    	ba.depositFromATM(1, 100);
    	printRes("depositFromATM", ba.getBalance() == 1199 && ba.getNTransactions() == 2);
    	
    	ba.withdraw(1, 100);
    	printRes("withdraw", ba.getBalance() == 1099 && ba.getNTransactions() == 3);
    	
    	ba.withdrawFromATM(1, 90);
    	printRes("withdrawFromATM", ba.getBalance() == 1008 && ba.getNTransactions() == 4);
    	
    	ba.deposit(2, 10);
    	printRes("deposit wrong id", ba.getBalance() == 1008 && ba.getNTransactions() == 4);
    	
    	ba.depositFromATM(2, 10);
    	printRes("deposit wrong id", ba.getBalance() == 1008 && ba.getNTransactions() == 4);
    	
    	ba.withdraw(2, 10);
    	printRes("withdraw wrong id", ba.getBalance() == 1008 && ba.getNTransactions() == 4);
    	
    	ba.withdrawFromATM(2, 10);
    	printRes("withdraw wrong id", ba.getBalance() == 1008 && ba.getNTransactions() == 4);
    	
    }
}
