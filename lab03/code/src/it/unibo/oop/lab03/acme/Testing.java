package it.unibo.oop.lab03.acme;

public final class Testing {

    private Testing() { }

    public static void main(final String[] args) {
        /*
         * 1) Creare 3 studenti a piacere 2) Creare 2 docenti a piacere 3)
         * Creare due aulee di esame, una con 100 posti una con 80 posti 4)
         * Creare due esami, uno con nMaxStudents=10, l'altro con nMaxStudents=2
         * 5) Iscrivere tutti e 3 gli studenti agli esami 6) Stampare in stdout
         * la rapresentazione in stringa dei due esami
         */
    	
    	Student st1 = new Student(1, "gagio", "zamagni", "shish", 2017);
    	Student st2 = new Student(2, "matteo", "valvu", "123456", 2017);
    	Student st3 = new Student(3, "matteo", "manzi", "passss", 2017);
    	
    	Professor pf1 = new Professor(1, "mario", "rossi");
    	Professor pf2 = new Professor(2, "luigi", "verdi");
    	
    	ExamRoom ro1 = new ExamRoom(100, "room 1", false, true);
    	ExamRoom ro2 = new ExamRoom(80, "room 2", true, true);
    	
    	Exam ex1 = new Exam(1, "programmazione", 10, pf1, ro1);
    	Exam ex2 = new Exam(2, "algoritmi", 2, pf2, ro2);
    	
    	ex1.registerStudent(st1);
    	ex1.registerStudent(st2);
    	ex2.registerStudent(st3);
    	
    	System.out.println(ex1);
    	System.out.println(ex2);
    }
}
