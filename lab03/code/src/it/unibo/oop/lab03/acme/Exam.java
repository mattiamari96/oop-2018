package it.unibo.oop.lab03.acme;

import java.util.Arrays;

public class Exam {
	private int id;
	private int maxStudents;
	private int registeredStudents;
	private String courseName;
	private Professor professor;
	private ExamRoom room;
	private Student[] students;
	
	public Exam(int id, String courseName, int maxStudents, Professor professor, ExamRoom room) {
		this.id = id;
		this.courseName = courseName;
		this.maxStudents = maxStudents;
		this.professor = professor;
		this.room = room;
		
		this.registeredStudents = 0;
		this.students = new Student[this.maxStudents];
	}

	public int getId() {
		return id;
	}

	public int getMaxStudents() {
		return maxStudents;
	}

	public int getRegisteredStudents() {
		return registeredStudents;
	}

	public String getCourseName() {
		return courseName;
	}

	public Professor getProfessor() {
		return professor;
	}

	public ExamRoom getRoom() {
		return room;
	}
	
	public void registerStudent(Student student) {
		if (this.registeredStudents == this.maxStudents) {
			return;
		}
		
		this.students[this.registeredStudents] = student;
		this.registeredStudents += 1;
	}
	
	public String toString() {
		return "Exam [ "
				+ "id=" + id + " "
				+ "name=" + courseName + " "
				+ "professor=" + professor + " "
				+ "room=" + room + " "
				+ "maxStudents=" + maxStudents + " "
				+ "registeredStudents=" + registeredStudents + " "
				+ "students=" + Arrays.toString(students) + " ]";
	}
}
