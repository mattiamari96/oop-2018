package it.unibo.oop.lab03.acme;

public class Professor implements User {
	
	private static final String DOT = ".";
	
	private int id;
	private String name;
	private String surname;
	private String password;
	private String[] courses;
	
	public Professor(int id, String name, String surname) {
		this.id = id;
		this.name = name;
		this.surname = surname;
		
		this.courses = new String[10];
	}

	public String getUsername() {
		return this.name + Professor.DOT + this.surname;
	}

	public String getPassword() {
		return this.password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

	public String getDescription() {
		return this.toString();
	}
	
	public void replaceCourse(String course, int index) {
		this.courses[index] = course;
	}
	
	public void replaceAllCourses(String[] courses) {
		this.courses = courses;
	}
	
	public String toString() {
		return "Professor [ "
				+ "id=" + this.id + " "
				+ "name=" + this.name + " "
				+ "surname=" + this.surname
				+ " ]";
	}
}
